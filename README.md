# tootssi

Tootssi is a gateway/bridge between IRC (irssi) and the Fediverse (Mastodon).

## Description

A perl script running in/with `irssi` to allow it to access **Mastodon** servers.

Currently in beta (i.e. heavy development, August 2023).

## Getting started

You probably need an uptodate Mastodon::Client (link to my version TODO).

## Authors and acknowledgment

- Gedge
- With thanks to Zigdon, for writing/starting twirssi - the inspiration for this project.

## License

FLOSS (TBC what flavour)
